import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GraphqlService {

 BASE_URL = 'https://api-euwest.graphcms.com/v1/ck6hnsr2ccl5j01d7201wg8x0/master';

  constructor(private client: HttpClient) { }

  getConcerts() {
    return this.client.post(this.BASE_URL, {
      query: `
      query { concertses (orderBy: horaires_ASC)
         { nom
          scenes
          artistes{
          nom}
          localisation{
            latitude
            longitude}
           horaires
           descriptions
            }
             }`
    }).pipe(map((r: any) => r.data));
  }
}
