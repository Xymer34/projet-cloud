import { Component, OnInit } from '@angular/core';
import { GraphqlService} from '../concert.service';
import {OpenweatherService} from '../weather.service';

@Component({
  selector: 'app-concert',
  templateUrl: './concert.component.html',
  styleUrls: ['./concert.component.css']
})
export class ConcertComponent implements OnInit {
  concertDatas : [];
  weatherDatas;

  constructor(private api: GraphqlService, private api2: OpenweatherService) {}

  ngOnInit() {
    this.loadConcerts();
  }
    loadConcerts() {
      this.api.getConcerts().subscribe(data => {
       this.concertDatas = data.concertses;
       this.concertDatas.forEach((concert:any) => {
         this.api2.getWeather(concert.localisation.latitude, concert.localisation.longitude).subscribe(
          data2 => {
            concert.weather = data2;
          }
         )
       })
       console.log(this.concertDatas);
  });
}
}

